#
# simple script to download and build g3d in-place
#
import sys
import os
import urllib2
import tarfile
import popen2

print "Connecting to http://downloads.sourceforge.net"
response = urllib2.urlopen("http://downloads.sourceforge.net/ecls/ecl-0.9l.tgz")
print "Connected"
ecl_src = open("ecl-0.9l.tgz","wb")
print "Downloading ECL.."
ecl_src.write( response.read() )
ecl_src.close()
print "Downloaded."
dir = os.getcwd()
targzipf = tarfile.open("ecl-0.9l.tgz", "r:gz")
targzipf.extractll()
targzipf.close()
os.chdir("ecl-0.9l")
(config_out, config_in) = popen2.popen2("./configure -prefix=" + dir)
for line in config_out:
    print line
(make_out, make_in) = popen2.popen2("make" + dir)
for line in config_out:
    print line
os.chdir("..")
# TO DO .. copy dlls to .exe dir
