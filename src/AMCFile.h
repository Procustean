#pragma once

class AMCFile
{
public:
	AMCFile(const std::string& filename);
	void read();	
	virtual ~AMCFile(void);
private:
	TextInput *m_textInput;
};
