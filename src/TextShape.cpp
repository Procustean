#include <G3D/G3DAll.h>
#include <GLG3D/GLG3D.h>
#include "TextShape.h"

using namespace G3D;

TextShape::TextShape(const std::string text, GFont::Ref font, float size) :
	string_(text),
	font_(font),
	size_(size)
{
	
}

 Shape::Type TextShape::type() const {
        return NONE;
}

void TextShape::render(class RenderDevice* rd,
					   const CoordinateFrame& cframe,
					   Color4 solidColor, 
					   Color4 wireColor)
{
	font_->draw3D(rd, string_, cframe, size_, solidColor, wireColor);
}

 float TextShape::area() const {
	G3D::Vector2 bounds = font_->bounds(string_, size_);
	return bounds.x * bounds.y;
}

 float TextShape::volume() const {
	G3D::Vector2 bounds = font_->bounds(string_, size_);
	return bounds.x * bounds.y;
}

 G3D::Vector3 TextShape::center() const {
	return G3D::Vector3(0.0, 0.0, 0.0);
}

 Sphere TextShape::boundingSphere() const {
	G3D::Vector2 bounds = font_->bounds(string_, size_);
	if (bounds.x > bounds.y)
		return Sphere(G3D::Vector3::zero(), bounds.x);
	else
		return Sphere(G3D::Vector3::zero(), bounds.y);
		
}

 AABox TextShape::boundingAABox() const
{
	G3D::Vector2 bounds = font_->bounds(string_, size_);
	return AABox(G3D::Vector3::zero(), G3D::Vector3::zero());
}

void TextShape::getRandomSurfacePoint(G3D::Vector3& P, G3D::Vector3& N) const
{
	P = G3D::Vector3::zero();
	N = G3D::Vector3::unitZ();
}

Vector3 TextShape::randomInteriorPoint() const
{
	return G3D::Vector3::zero();
}
