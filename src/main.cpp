
#include <G3D/G3DAll.h>
#include <GLG3D/GLG3D.h>
#include "ecl/ecl.h"
#include "App.h"
#include "Entity.h"

#if defined(G3D_VER) && (G3D_VER < 70000)
#   error Requires G3D 7.00
#endif

G3D_START_AT_MAIN();

App* g_theApp = NULL;


static cl_object cl_write_text_to_g3d(cl_object array)
{
	char c;
	GConsole::string to_write;

	cl_object i_index = ecl_make_unsigned_integer(0);
	cl_object dim = cl_array_dimension(array, i_index);
	cl_fixnum dim_fixnum = ecl_to_fixnum( dim );
	cl_object i_fixnum = ecl_make_unsigned_integer(0);  
	for( int i = 0; i < dim_fixnum; i++ )
	{
		i_fixnum = ecl_make_unsigned_integer(i);
		cl_object ecl_char = cl_row_major_aref(array, i_fixnum);
	   
		// possible encoding nightmare ahoy!
		// Need to transcode as whatever g3d uses
		if ((CHARACTERP(ecl_char)) && (cl_graphic_char_p(ecl_char))) {
			char c = ecl_to_char(ecl_char);
			to_write.push_back(c);
		}
	}
	if (g_theApp != NULL) 
		g_theApp->console->print(to_write);
	return Cnil;
}

inline cl_object symbol(char *str)
{
	return cl_intern(1, make_simple_base_string(str));
}


cl_object g_eval_handler;

cl_object g_readErrorSymbol;
cl_object g_evalErrorSymbol;

bool isError(cl_object obj, const char ** errorMessage)
{
    if (errorMessage)
    {
        if (obj == g_readErrorSymbol)
        {
            *errorMessage = "Error reading form";
        }
        else if (obj == g_evalErrorSymbol)
        {
            *errorMessage = "Error evaluating form";
        }
    }
    return obj == g_readErrorSymbol || obj == g_evalErrorSymbol;
}

// Construct a function call to safely do the read operation.
cl_object safeRead(const std::string & strCmd)
{
    cl_object command = make_simple_base_string((char*)strCmd.c_str());
    cl_object readFromStringSymbol = c_string_to_object("READ-FROM-STRING");
    // The form looks like this:
    //  ( 'read-from-string . ( <our command> . nil ) )
    // By constructing it manually we don't have to worry about escaping.
    cl_object form = CONS(readFromStringSymbol,
                          CONS(command, Cnil));
    // Evaluate the built up read form.
    cl_object read = si_safe_eval(3, form, Cnil, g_readErrorSymbol);
    return read;
}

int main(int argc, char** argv) {

    GApp::Settings settings;

	const char *handler = "#.(lambda (str env err-value) (multiple-value-list (catch 'si::protect-tag (let* ((*debugger-hook* #'(lambda (condition old-hooks) (throw 'si::protect-tag condition)))) (si::eval-with-env (read-from-string str) env)))))";

	cl_boot(argc, argv);    

	ecl_register_static_root(&g_eval_handler);
	g_eval_handler = c_string_to_object(handler);

	g_readErrorSymbol = cl_gensym(0);
    g_evalErrorSymbol = cl_gensym(0);
	
	si_select_package(make_simple_base_string("CL-USER"));
	
	cl_def_c_function(symbol("WRITE-TEXT-TO-G3D"), /* uppercase! */
					  (void *)cl_write_text_to_g3d,
					  1);

	cl_def_c_function(symbol("MAKE-SPHERE-ENTITY"),
					  (void *) cl_make_sphere_entity,
					  1);

	cl_def_c_function(symbol("MAKE-BOX-ENTITY"),
					  (void *) cl_make_box_entity,
					  3);
	
	cl_def_c_function(symbol("MAKE-AXES-ENTITY"),
					  (void *) cl_make_axes_entity,
					  0);

	cl_def_c_function(symbol("MAKE-TRIANGLE-ENTITY"),
					  (void *) cl_make_triangle_entity,
					  9);

	cl_def_c_function(symbol("MAKE-POINT-ENTITY"),
					  (void *) cl_make_point_entity,
					  3);
	
	cl_def_c_function(symbol("ENTITY-ORIENTATION"),
					  (void *) cl_get_entity_orientation,
					  1);

	cl_def_c_function(symbol("ENTITY-POSITION"),
					  (void *) cl_get_entity_position,
					  1);

	cl_def_c_function(symbol("SET-ENTITY-ORIENTATION"),
					  (void *) cl_set_entity_orientation,
					  5);

	cl_def_c_function(symbol("SET-ENTITY-POSITION"),
					  (void *) cl_set_entity_position,
					  4);

	cl_def_c_function(symbol("GET-ENTITY-WIRE-COLOUR"),
					  (void*) cl_set_entity_wire_color,
					  1);
	
	cl_def_c_function(symbol("SET-ENTITY-WIRE-COLOUR"),
					  (void*) cl_set_entity_wire_color,
					  5);

	cl_def_c_function(symbol("GET-ENTITY-SOLID-COLOUR"),
					  (void*) cl_set_entity_solid_color,
					  1);
	
	cl_def_c_function(symbol("SET-ENTITY-SOLID-COLOUR"),
					  (void*) cl_set_entity_solid_color,
					  5);

	cl_def_c_function(symbol("ENTITY-YAW"),
					  (void*) cl_entity_yaw,
					  2);
	
	cl_def_c_function(symbol("ENTITY-PITCH"),
					  (void*) cl_entity_pitch,
					  2);
	
	cl_def_c_function(symbol("ENTITY-ROLL"),
					  (void*) cl_entity_roll,
					  2);

	cl_def_c_function(symbol("ENTITY-FORWARD"),
					  (void*) cl_entity_forward,
					  2);

	cl_def_c_function(symbol("ENTITY-BACKWARD"),
					  (void*) cl_entity_backward,
					  2);

	cl_def_c_function(symbol("ENTITY-UP"),
					  (void*) cl_entity_up,
					  2);

	cl_def_c_function(symbol("ENTITY-DOWN"),
					  (void*) cl_entity_down,
					  2);

	cl_def_c_function(symbol("ENTITY-LEFT"),
					  (void*) cl_entity_left,
					  2);

	cl_def_c_function(symbol("ENTITY-RIGHT"),
					  (void*) cl_entity_right,
					  2);
	
		
	
	g_theApp = new App(settings);
	g_theApp->run();
	delete g_theApp;
  

	cl_shutdown();
}
