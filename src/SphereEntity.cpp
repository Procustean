
#include <G3D/G3DAll.h>
#include <GLG3D/GLG3D.h>
#include "ecl/ecl.h"
#include "Entity.h"
#include "SphereEntity.h"

using namespace G3D;

SphereEntity::SphereEntity(const Vector3& center, float radius)
: sphere_(center, radius)
{
}

void SphereEntity::render(RenderDevice* renderer)
{
	Draw::sphere(sphere_, renderer);
}

SphereEntity::~SphereEntity(void)
{
}
