#pragma once

class AMCNode
{
public:
	AMCNode(void);
	~AMCNode(void);

	enum Channel 
	{
		TX, TY, TZ, RX, RY, RZ, LENGTH
	};

	enum Axes
	{ 
		XYZ, ZYX, YXZ, YZX
	};

	class Limit
	{
	public:
		Channel channel;
		std::pair<double, double> limits;
	};

	int id;

	std::string name;
	double direction[3];
	double length;
	G3D::Quat axis;
	std::vector<Channel> channels;
	std::vector<Limit> limits;
};

class AMCRoot : public AMCNode
{
public:

	std::vector<Axes> axis;
	std::vector<Channel> order;
	G3D::Vector3 m_position;
	G3D::Quat m_orientation;
};