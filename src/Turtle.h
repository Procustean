
#pragma once

class Turtle : public G3D::ReferenceCountedObject
{
private:
	CoordinateFrame state;
	
public:
	void up(float value);
	void left(float value);
	void forward(float value);

	void pitch(float value);
	void roll(float value);
	void yaw(float value);

	enum TurtleCommand
	{
		eUP,
		eDOWN,
		eLEFT,
		eRIGHT,
		eFORWARD,
		eBACKWARD
	};
	void onGraphics(RenderDevice* rd);
	void onConsoleCommand(TurtleCommand tc, float arg);
	Turtle();
	virtual ~Turtle();
};


typedef G3D::ReferenceCountedPointer<class Turtle> TurtleRef;
