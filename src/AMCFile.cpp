#include <G3D/G3DAll.h>
#include <GLG3D/GLG3D.h>
#include "AMCFile.h"

AMCFile::AMCFile(const std::string& filename)
{
	TextInput::Settings amcSettings;
    amcSettings.cComments = false;
    amcSettings.cppComments = false;
	amcSettings.msvcSpecials = false;
	amcSettings.otherCommentCharacter = '#';
	amcSettings.trueSymbols.clear();
	amcSettings.falseSymbols.clear();
	amcSettings.caseSensitive = true;
	m_textInput = new TextInput(filename);
}

void AMCFile::read()
{
	Token t;
	while (m_textInput->hasMore())
	{
		t = m_textInput->read();
		switch (t.type()) {
			case Token::BOOLEAN_TYPE:
				std::cerr << "boolean: ";
				std::cerr << t.boolean();
				break;

			case Token::DOUBLE_QUOTED_TYPE:
				std::cerr << "double quoted: ";
 				std::cerr << t.string();
				break;

			case Token::SINGLE_QUOTED_TYPE:
				std::cerr << "single quoted: ";
				std::cerr << t.string();
				break;

			case Token::FLOATING_POINT_TYPE:
				std::cerr << "floating point: ";
				std::cerr << t.number();
				break;

			case Token::INTEGER_TYPE:
				std::cerr << "integer: ";
				std::cerr << t.number();
				break;

			case Token::END_TYPE:
				std::cerr << "end: ";
				break;

			case Token::SYMBOL_TYPE:
				std::cerr << "symbol type: ";
				std::cerr << t.string();
				break;
		}
		std::cerr << std::endl;
	}
	return;
}

AMCFile::~AMCFile(void)
{
}
