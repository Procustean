
#ifndef ENTITY_H_INCLUDED
#define ENTITY_H_INCLUDED

namespace G3D
{

// these two virtual functions are unimplemented in G3D-7.00 because of a typo.
class TurtleAxesShape : public AxesShape
{
public:
    TurtleAxesShape(const G3D::CoordinateFrame&a) : AxesShape(a)
    {
    }
    
    virtual G3D::Sphere boundingSphere() const
    {
        return Sphere(center(), 1);
    }
    
    virtual G3D::AABox boundingAABox() const
    {
        debugAssertM(false, "No bounding axis aligned box for axes.");
        static AABox aab;
        return aab;
    }
};

// these two virtual functions are unimplemented in G3D-7.00 because of a typo.
class TurtleRayShape : public RayShape
{
public:
    TurtleRayShape(const G3D::Ray&r) : RayShape(r)
    {
    }
    
    virtual G3D::Sphere boundingSphere() const
    {
        return Sphere(center(), 1);
    }
    
    virtual G3D::AABox boundingAABox() const
    {
        debugAssertM(false, "No bounding axis aligned box for axes.");
        static AABox aab;
        return aab;
    }
};

class TriangleShape : public Shape
{
	G3D::Triangle geometry;

public:
	TriangleShape(const G3D::Vector3& v0, const G3D::Vector3& v1, const G3D::Vector3& v2) : geometry(v0, v1, v2)
	{
	}
	
	virtual Type type() const { return PLANE; };

	virtual float volume() const
	{
		return 0.0f;
	}	

	Vector3 center() const
	{
		return geometry.center();
	}
	
	
	virtual float area() const
	{
		return geometry.area();
	}
	
    virtual G3D::Sphere boundingSphere() const
    {
        return Sphere(center(), 1);
    }
    
    virtual G3D::AABox boundingAABox() const
    {
		G3D::AABox result;
		geometry.getBounds(result);
		return result;
			
    }

	void getRandomSurfacePoint(Vector3& P, Vector3& N = Vector3::dummy) const
	{
		P = geometry.randomPoint();
		N = geometry.normal();
		return;
	};

	Vector3 randomInteriorPoint() const
	{
		return geometry.randomPoint();
	};

	void render(RenderDevice* rd,
				const CoordinateFrame& cframe,
				Color4 solidColor,
				Color4 wireColor)
	{
		CoordinateFrame cframe0 = rd->getObjectToWorldMatrix();

		rd->pushState();
        rd->setObjectToWorldMatrix(cframe0 * cframe);
        if (solidColor.a < 1.0) {
            rd->setBlendFunc(RenderDevice::BLEND_SRC_ALPHA, RenderDevice::BLEND_ONE_MINUS_SRC_ALPHA);
        }
        rd->setColor(solidColor);
        rd->beginPrimitive(RenderDevice::TRIANGLES);
		const Vector3 v0 = geometry.vertex(0);
		const Vector3 v1 = geometry.vertex(1);				
		const Vector3 v2 = geometry.vertex(2);
		rd->sendVertex(v0);
		rd->sendVertex(v1);
		rd->sendVertex(v2);
        rd->endPrimitive();

        rd->setBlendFunc(RenderDevice::BLEND_SRC_ALPHA, RenderDevice::BLEND_ONE_MINUS_SRC_ALPHA);
        rd->setLineWidth(2);
        rd->setColor(wireColor);
        rd->beginPrimitive(RenderDevice::LINES);
		rd->sendVertex(v0);
		rd->sendVertex(v1);
		rd->sendVertex(v1);
		rd->sendVertex(v2);
		rd->sendVertex(v2);
		rd->sendVertex(v0);
        rd->endPrimitive();
		rd->popState();	
	}
};

} // end namespece G3D





	
    
typedef cl_fixnum EntityHandle;


class Entity 
{
protected:	
	Entity();
	~Entity();
	static EntityHandle lastHandle;
	static G3D::Table<EntityHandle, Entity* > entities;

public:
	G3D::CoordinateFrame at;
	G3D::Color4 solidColor;
	G3D::Color4 wireColor;
	G3D::ShapeRef myShape;
	EntityHandle handle;
    bool selected;
	
	static EntityHandle create();
	static Entity* get(EntityHandle h);
	
	void render(G3D::RenderDevice* rd)
	{
	    myShape->render(rd, at, solidColor, wireColor);
	};
	
	void update(float dt);
	
	static void renderEntities(G3D::RenderDevice*);
	static void updateEntities(float dt);

	
};

void Render(EntityHandle handle, G3D::RenderDevice *rd);
void Update(EntityHandle handle, float dt);

//void EntityHandle updateHook(..); hook update to lisp function
//void EndityHandle renderHook(..); hook render to lisp function

extern cl_object cl_make_sphere_entity(cl_object radius);
extern cl_object cl_make_axes_entity(void);
extern cl_object cl_make_box_entity(cl_object x, cl_object y, cl_object z);
extern cl_object cl_make_triangle_entity(cl_object x0, cl_object y0, cl_object z0,
										 cl_object x1, cl_object y1, cl_object z1,
										 cl_object x2, cl_object y2, cl_object z2);
extern cl_object cl_make_point_entity(cl_object x, cl_object y, cl_object z);

extern cl_object cl_make_mesh_entity(cl_object vertices, cl_object indices);

extern cl_object cl_set_entity_position(cl_object handle, cl_object x, cl_object y, cl_object z);
extern cl_object cl_get_entity_position(cl_object handle);

extern cl_object cl_set_entity_orientation(cl_object handle, cl_object x, cl_object y, cl_object z, cl_object angle);
extern cl_object cl_get_entity_orientation(cl_object handle);

extern cl_object cl_set_entity_solid_color(cl_object handle, cl_object r, cl_object g, cl_object b, cl_object a);
extern cl_object cl_get_entity_solid_color(cl_object handle);

extern cl_object cl_set_entity_wire_color(cl_object handle, cl_object r, cl_object g, cl_object b, cl_object a);
extern cl_object cl_get_entity_wire_color(cl_object handle);

extern cl_object cl_entity_yaw(cl_object handle, cl_object value);
extern cl_object cl_entity_pitch(cl_object handle, cl_object value);
extern cl_object cl_entity_roll(cl_object handle, cl_object value);
extern cl_object cl_entity_forward(cl_object handle, cl_object value);
extern cl_object cl_entity_backward(cl_object handle, cl_object value);
extern cl_object cl_entity_up(cl_object handle, cl_object value);
extern cl_object cl_entity_down(cl_object handle, cl_object value);
extern cl_object cl_entity_left(cl_object handle, cl_object value);
extern cl_object cl_entity_right(cl_object handle, cl_object value);
extern cl_object cl_entity_mark_point(cl_object handle);
extern cl_object cl_entity_mark_line(cl_object handle);
extern cl_object cl_entity_mark_triangle(cl_object handle);
extern cl_object cl_object_mark_colour(cl_object handle, cl_object r, cl_object g, cl_object b, cl_object a);

// extern cl_object cl_entity_scale(cl_object handle, cl_object x, cl_object y, cl_object z)
// extern cl_object cl_import_mesh_entity(cl_object handle, cl_object name)
// extern cl_object cl_clone_child_entity(cl_object handle); // create a child entity of the same type as the parent, (uses relative coordspace?)

#endif
