#include <G3D/G3DAll.h>
#include <GLG3D/GLG3D.h>
#include "TextShape.h"
#include "ecl/ecl.h"
#include "Entity.h"

EntityHandle Entity::lastHandle = 0x1;

G3D::Table<EntityHandle, Entity*> Entity::entities;

Entity::Entity(void) : solidColor(1.0f, 0.0f, 0.0f, 1.0f), wireColor(0.0f, 0.0f, 0.0f, 0.5f), selected(false)
{
	handle = Entity::lastHandle;
	lastHandle++;
	entities.set(handle, this);
}

EntityHandle Entity::create()
{
	Entity *result = new Entity();	
	return result->handle;
}

Entity *Entity::get(EntityHandle h)
{
	return Entity::entities[h];
}

void Entity::renderEntities(G3D::RenderDevice* rd)
{
    G3D::Table<EntityHandle, Entity*>::Iterator it = Entity::entities.begin();
	for( ; it != Entity::entities.end(); ++it )
		it->value->render(rd);
}

void Entity::updateEntities(float dt)
{
}


cl_object cl_set_entity_position(cl_object handle, cl_object x, cl_object y, cl_object z)
{
	Entity *ent = Entity::get(fix(handle));
	if (ent != NULL) {
		ent->at.translation = G3D::Vector3(ecl_to_float(x),ecl_to_float(y),ecl_to_float(z));
		return Ct;
	}
	return Cnil;	
}

cl_object cl_get_entity_position(cl_object handle)
{
	Entity *ent = Entity::get(fix(handle));
	if (ent != NULL) {
		float x = ent->at.translation.x;
		float y = ent->at.translation.y;
		float z = ent->at.translation.z;

		NVALUES=3;
		VALUES(0) = ecl_make_singlefloat(x);
		VALUES(1) = ecl_make_singlefloat(y);
		VALUES(2) = ecl_make_singlefloat(z);
		return (VALUES(0));
	}
	return Cnil;
}


cl_object cl_get_entity_orientation(cl_object handle)
{
	Entity *ent = Entity::get(ecl_to_fixnum(handle));
	if (ent != NULL)
	{
		G3D::Quat q(ent->at.rotation);
		G3D::Vector3 axis;
		float angle;
		q.toAxisAngleRotation( axis, angle );
		
		NVALUES=4;		
		VALUES(0) = ecl_make_singlefloat(axis.x);
		VALUES(1) = ecl_make_singlefloat(axis.y);
		VALUES(2) = ecl_make_singlefloat(axis.z);
		VALUES(3) = ecl_make_singlefloat(angle);
		return VALUES(0);
	}
	return Cnil;
}

cl_object cl_set_entity_orientation(cl_object handle, cl_object x, cl_object y, cl_object z, cl_object a)
{
	Entity *ent = Entity::get(fix(handle));
	if (ent != NULL)
	{
		Vector3 axis( ecl_to_float(x), ecl_to_float(y), ecl_to_float(z) );
		float angle = ecl_to_float(a);
		ent->at.rotation = Matrix3(G3D::Quat::fromAxisAngleRotation( axis, angle ));
		return Ct;
	}
	return Cnil;	
}

cl_object cl_make_sphere_entity(cl_object radius)
{
	EntityHandle handle = Entity::create();
	Entity *ent = Entity::get(handle);
	ent->myShape = new G3D::SphereShape( ecl_to_float(radius) );
	return MAKE_FIXNUM(handle);
}

cl_object cl_make_axes_entity()
{
	EntityHandle handle = Entity::create();
	Entity *ent = Entity::get(handle);
	G3D::CoordinateFrame frame;
	ent->myShape = new G3D::TurtleAxesShape( frame );
	return MAKE_FIXNUM(handle);
}

cl_object cl_make_box_entity(cl_object x, cl_object y, cl_object z)
{
	EntityHandle handle = Entity::create();
	Entity *ent = Entity::get(handle);
	ent->myShape = new G3D::BoxShape( ecl_to_float(x), ecl_to_float(y), ecl_to_float(z) );
	return MAKE_FIXNUM(handle);
}

cl_object cl_make_ray_entity(cl_object x, cl_object y, cl_object z, cl_object nx, cl_object ny, cl_object nz)
{
	EntityHandle handle = Entity::create();
	Entity *ent = Entity::get(handle);
	G3D::Vector3 origin( ecl_to_float(x), ecl_to_float(y), ecl_to_float(z) );
	G3D::Vector3 direction( ecl_to_float(nx), ecl_to_float(ny), ecl_to_float(nz) );   
	G3D::Ray r(G3D::Ray::fromOriginAndDirection(origin, direction));
	ent->myShape = new G3D::TurtleRayShape( r  );	
	return MAKE_FIXNUM(handle);
}

cl_object cl_make_triangle_entity(cl_object x0, cl_object y0, cl_object z0,
								  cl_object x1, cl_object y1, cl_object z1,
								  cl_object x2, cl_object y2, cl_object z2)
{
	EntityHandle handle = Entity::create();
	Entity *ent = Entity::get(handle);
	G3D::Vector3 v0( ecl_to_float(x0), ecl_to_float(y0), ecl_to_float(z0) );
	G3D::Vector3 v1( ecl_to_float(x1), ecl_to_float(y1), ecl_to_float(z1) );
	G3D::Vector3 v2( ecl_to_float(x2), ecl_to_float(y2), ecl_to_float(z2) );	
	ent->myShape = new G3D::TriangleShape( v0, v1, v2 );
	return MAKE_FIXNUM(handle);	
}

cl_object cl_make_point_entity(cl_object x, cl_object y, cl_object z)
{
	EntityHandle handle = Entity::create();
	Entity *ent = Entity::get(handle);
	G3D::Vector3 origin( ecl_to_float(x), ecl_to_float(y), ecl_to_float(z) );
	ent->myShape = new G3D::PointShape( origin );
	return MAKE_FIXNUM(handle);	
}

cl_object cl_make_mesh_entity(cl_object vertices, cl_object indices)
{
	// NYI -- needs thought
	return Cnil;
}

cl_object cl_set_entity_solid_color(cl_object handle, cl_object r, cl_object g, cl_object b, cl_object a)
{
	Entity *ent = Entity::get(fix(handle));
	if (ent != NULL)
	{
		ent->solidColor = Color4( ecl_to_float(r), ecl_to_float(g), ecl_to_float(b), ecl_to_float(a) );
		return Ct;
	}
	return Cnil;	
}

cl_object cl_get_entity_solid_color(cl_object handle)
{
	Entity *ent = Entity::get(ecl_to_fixnum(handle));
	if (ent != NULL)
	{
		NVALUES=4;		
		VALUES(0) = ecl_make_singlefloat(ent->solidColor.r);
		VALUES(1) = ecl_make_singlefloat(ent->solidColor.g);
		VALUES(2) = ecl_make_singlefloat(ent->solidColor.b);
		VALUES(3) = ecl_make_singlefloat(ent->solidColor.a);
		return VALUES(0);
	}
	return Cnil;
	
}

cl_object cl_set_entity_wire_color(cl_object handle, cl_object r, cl_object g, cl_object b, cl_object a)
{
	Entity *ent = Entity::get(fix(handle));
	if (ent != NULL)
	{
		ent->wireColor = Color4( ecl_to_float(r), ecl_to_float(g), ecl_to_float(b), ecl_to_float(a) );
		return Ct;
	}
	return Cnil;		
}

cl_object cl_get_entity_wire_color(cl_object handle)
{
	Entity *ent = Entity::get(ecl_to_fixnum(handle));
	if (ent != NULL)
	{
		NVALUES=4;		
		VALUES(0) = ecl_make_singlefloat(ent->wireColor.r);
		VALUES(1) = ecl_make_singlefloat(ent->wireColor.g);
		VALUES(2) = ecl_make_singlefloat(ent->wireColor.b);
		VALUES(3) = ecl_make_singlefloat(ent->wireColor.a);
		return VALUES(0);
	}
	return Cnil;
}


cl_object cl_entity_yaw(cl_object handle, cl_object value)
{
	Entity *ent = Entity::get(ecl_to_fixnum(handle));
	if (ent != NULL)
	{
		float yaw_value = ecl_to_float(value);
		CoordinateFrame yaw(Matrix3::fromAxisAngle(ent->at.upVector(), yaw_value));
		ent->at = ent->at * yaw;
		return ecl_make_singlefloat( yaw_value );						
	} else {	
		return Cnil;
	}
}

cl_object cl_entity_pitch(cl_object handle, cl_object value)
{
	Entity *ent = Entity::get(ecl_to_fixnum(handle));
	if (ent != NULL)
	{
		float pitch_value = ecl_to_float(value);
		CoordinateFrame pitch(Matrix3::fromAxisAngle(ent->at.leftVector(), pitch_value));
		ent->at = ent->at * pitch;
		return ecl_make_singlefloat( pitch_value );				
	} else {
		return Cnil;
	}
}

cl_object cl_entity_roll(cl_object handle, cl_object value)
{
	Entity *ent = Entity::get(ecl_to_fixnum(handle));
	if (ent != NULL)
	{
		float roll_value = ecl_to_float(value);
		CoordinateFrame roll(Matrix3::fromAxisAngle(ent->at.lookVector(), roll_value));
		ent->at = ent->at * roll;
		return ecl_make_singlefloat( roll_value );		
	} else {
		return Cnil;
	}
	
}

cl_object cl_entity_forward(cl_object handle, cl_object value)
{
	Entity *ent = Entity::get(ecl_to_fixnum(handle));
	if (ent != NULL)
	{
		float forward_value = ecl_to_float(value);
		ent->at = ent->at + ( ent->at.lookVector() * forward_value );
		return ecl_make_singlefloat( forward_value );
	} else {
		return Cnil;
	}
}

cl_object cl_entity_backward(cl_object handle, cl_object value)
{
	Entity *ent = Entity::get(ecl_to_fixnum(handle));
	if (ent != NULL)
	{
		float forward_value = ecl_to_float(value);
		ent->at = ent->at + ( ent->at.lookVector() * -forward_value );
		return ecl_make_singlefloat( forward_value );		
	} else {
		return Cnil;
	}
}

cl_object cl_entity_up(cl_object handle, cl_object value)
{
	Entity *ent = Entity::get(ecl_to_fixnum(handle));
	if (ent != NULL)
	{
		float forward_value = ecl_to_float(value);
		ent->at = ent->at + ( ent->at.upVector() * forward_value );
		return ecl_make_singlefloat( forward_value );		
	} else {
		return Cnil;
	}
}

cl_object cl_entity_down(cl_object handle, cl_object value)
{
	Entity *ent = Entity::get(ecl_to_fixnum(handle));
	if (ent != NULL)
	{
		float forward_value = ecl_to_float(value);
		ent->at = ent->at + ( ent->at.upVector() * -forward_value );
		return ecl_make_singlefloat( forward_value );		
	} else {
		return Cnil;
	}
}

cl_object cl_entity_left(cl_object handle, cl_object value)
{
	Entity *ent = Entity::get(ecl_to_fixnum(handle));
	if (ent != NULL)
	{
		float forward_value = ecl_to_float(value);
		ent->at = ent->at + ( ent->at.leftVector() * forward_value );
		return ecl_make_singlefloat( forward_value );		
	} else {
		return Cnil;
	}

}

cl_object cl_entity_right(cl_object handle, cl_object value)
{
	Entity *ent = Entity::get(ecl_to_fixnum(handle));
	if (ent != NULL)
	{
		float forward_value = ecl_to_float(value);
		ent->at = ent->at + ( ent->at.leftVector() * -forward_value );
		return ecl_make_singlefloat( forward_value );
	} else {
		return Cnil;
	}
}


						 
Entity::~Entity(void)
{
	entities.remove(handle);
}
