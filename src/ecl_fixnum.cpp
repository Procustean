
namespace Ecl
{

class Fixnum {

private:
    cl_fixnum the_fixnum;

    Fixnum();
public:

    Fixnum(int i) : the_fixnum( MAKE_FIXNUM(i) )
    {
    }

    Fixnum(long l) : the_fixnum( MAKE_FIXNUM(i) )
    {
    }
        
    Fixnum(const cl_object& obj) : 
    {
        the_fixnum = other.the_finxum;
    }

    inline operator int() const
    {
#ifdef NDEBUG        
        return fix(the_fixnum);
#else
        return fixint(the_fixnum);
#endif        
    }

    inline operator long() const
    {
#ifdef NDEBUG        
        return fix(the_fixnum);
#else
        return fixint(the_fixnum);
#endif        
    }

    
};

class SingleFloat
{
private:
    cl_object my_float;
    
    SingleFloat();
    
public:
    SingleFloat(float f) : my_float(ecl_make_singlefloat(f))
    {
    }

    SingleFloat(const SingleFloat& other)
    {
        my_float = other.my_float;
    }
    
    inline operator float() const {
        ecl_to_float(my_float);
    }

    inline operator cl_object() const {
        return my_float;
    }
};


class String
{
private:
    cl_string the_string;
public:
    String(std::string s) : my_string(s)
    {
        the_string = make_simple_string(s.c_str());
    }

    String(const String& other)
    {
        my_string = other.my_string();
        the_string = make_simple_string(s.c_str());
    }
    
    inline operator std::string() const
    {
        return std::string(the_string->self);
    }

    inline operator cl_string()
    {
        return the_string;
    }
};


// to do -- maybe a template class specialised on differen types
class Vector()
{
private:    
    cl_object  my_array;

public:

    Vector(std::size_t size, cl_eltype type)
    {
        my_array = si_make_vector(ecl_elttype_to_symbol(type), MAKE_FIXNUM(dim), Cnil, MAKE_FIXNUM(0), Cnil, MAKE_FIXNUM(0));
    }
        
    inline std::size_t size() const
    {
        return my_array->dim;
    }
    
    inline cl_eltype elements() const
    {
        return array_elttype(my_array);
    }
    
    inline cl_object operator[](cl_index index) const
    {
        return aref1(my_array, index);
    }

    // gratituous abuse of operator overloadin
    inline cl_object operator()(cl_index index, cl_object value)
    {
        return aset1(my_array, index, value);
    }

    inline  operator cl_object() const
    {
        return my_array;
    }
}

}
