
#ifndef TEXT_SHAPE_H_DEFINED
#define TEXT_SHAPE_H_DEFINED

class TextShape : public Shape
{
	std::string string_;
	GFont::Ref font_;
	float size_;
	
public:

	TextShape(const std::string text, GFont::Ref font, float size = 0.1f);


	virtual Shape::Type type() const;
	
	virtual void render(class RenderDevice* rd,
						const CoordinateFrame& cframe,
						Color4 solidColor = Color4(.5,.5,0,.5), 
						Color4 wireColor = Color3::black());

    /** Surface area of the outside of this object. */
    virtual float area() const = 0;

    /** Volume of the interior of this object. */
    virtual float volume() const = 0;

    /** Center of mass for this object. */
    virtual Vector3 center() const = 0;

    /** Bounding sphere of this object. */
    virtual Sphere boundingSphere() const = 0;

    /** Bounding axis aligned box of this object. */
    virtual AABox boundingAABox() const = 0;

    /** A point selected uniformly at random with respect to the
        surface area of this object.  Not available on the Plane or
        Ray, which have infinite extent.  The normal has unit length
        and points out of the surface. */
    virtual void getRandomSurfacePoint(Vector3& P, 
                                       Vector3& N = Vector3::dummy) const = 0;

    /** A point selected uniformly at random with respect to the
        volume of this object.  Not available on objects with infinite
        extent.*/
    virtual Vector3 randomInteriorPoint() const = 0;
	
};

#endif
