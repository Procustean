
#include "App.h"
#include "Turtle.h"

Turtle::Turtle() : state(Vector3(0, 4,0))
{
}

Turtle::~Turtle()
{
}

void Turtle::up(float value)
{
	state = state + ( state.upVector() * value );
}

void Turtle::left(float value)
{
	state = state + ( state.leftVector() * value );
}

void Turtle::forward(float value)
{
	state = state + ( state.lookVector() * -value );		
}

void Turtle::pitch(float value)
{
	CoordinateFrame pitch(Matrix3::fromAxisAngle(state.leftVector(), value));
	state = state * pitch;
}

void Turtle::roll(float value)
{
	CoordinateFrame pitch(Matrix3::fromAxisAngle(state.lookVector(), value));
	state = state * pitch;
}

void Turtle::yaw(float value)
{
	CoordinateFrame pitch(Matrix3::fromAxisAngle(state.upVector(), value));
	state = state * pitch;
}

void Turtle::onGraphics(RenderDevice *rd)
{
	Draw::axes(state, rd);
}

void Turtle::onConsoleCommand(TurtleCommand tc, float arg)
{
}
