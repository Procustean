;;; -*- mode: lisp; syntax: common-lisp -*-

;;; Gray streams require ECL 0.9j or later.
(defclass g3d-console-view-stream (gray:fundamental-character-output-stream) 
  ((buffer :accessor buffer-of :initform (make-array (list 0) :adjustable t :fill-pointer 0))))

(defmethod gray:stream-line-column ((stream g3d-console-view-stream)) 
  (fill-pointer (buffer-of stream)))

(defmethod gray:stream-advance-to-column ((stream g3d-console-view-stream) column)
  nil)
  
(defmethod gray:stream-write-char ((stream g3d-console-view-stream) char)
  (declare (ignore stream))
  (vector-push-extend char (buffer-of stream)))

(defmethod gray:stream-terpri ((stream g3d-console-view-stream))
  (write-text-to-g3d (buffer-of stream))
  (setf (buffer-of stream) (make-array 0 :adjustable t :fill-pointer 0)))

(defvar *g3d-console-view-stream* (make-instance 'g3d-console-view-stream))

(setf *standard-output* *g3d-console-view-stream*)
(setf *error-output* *g3d-console-view-stream*)
(setf *trace-output* *g3d-console-view-stream*)

;; (defun one-of (choices &optional (prompt "Choice"))
;;    (let ((n (length choices)) (i))
;;      (do ((c choices (cdr c)) (i 1 (+ i 1)))
;;          ((null c))
;;        (format t "~&[~D] ~A~%" i (car c)))
;;      (do () ((typep i `(integer 1 ,n)))
;;        (format t "~&~A: " prompt)
;;        (setq i (read))
;;        (fresh-line))
;;      (nth (- i 1) choices)))


;; (defun my-debugger (condition me-or-my-encapsulation)
;;   (format t "Jim, it's code, but not as we know it. ~A~&" condition)
;;   nil)

;;   (let (restart (one-of (compute-restarts)))
;; 	(if (not restart) (error "My debugger got an error"))
;; 	(let ((*debugger-hook* me-or-my-encapsulation))
;; 	  (invoke-restart-interactively restart))))

;;(setf *debugger-hook* #'my-debugger)

;; alas C++ does sometimes generate these conditions and then ecl
;; traps them inappropiately
(si:trap-fpe 'FLOATING-POINT-UNDERFLOW NIL)
(si:trap-fpe 'FLOATING-POINT-OVERFLOW NIL)
(si:trap-fpe 'DIVISION-BY-ZERO NIL)

(format t "Welcome to ECL / G3D Test!~%")

(defconstant +none+ 0)
(defconstant +mesh+ 1)
(defconstant +box+ 2)
(defconstant +cylinder+ 3)
(defconstant +sphere+ 4)
(defconstant +ray+ 5)
(defconstant +capsule+ 6)
(defconstant +plane+ 7)
(defconstant +axes+ 8)
(defconstant +point+ 9)
