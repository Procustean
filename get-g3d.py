#
# simple script to download and build g3d in-place
#
import sys
import os
import urllib2
import zipfile
import popen2

print "Connecting to http://downloads.sourceforge.net"
response = urllib2.urlopen("http://downloads.sourceforge.net/g3d-cpp/G3D-7.00-src.zip")
print "Connected"
g3d_src = open("G3D-7.00-src.zip","wb")
print "Downloading G3D.."
g3d_src.write( response.read() )
g3d_src.close()
print "Downloaded."
response.close()
dir = os.getcwd()
zipf = open("G3D-7.00-src.zip", "rb")
zip = zipfile.ZipFile(zipf)
for name in zip.namelist():
    print "Extracting " + name
    if name.endswith('/'):
        os.mkdir(os.path.join(os.getcwd(),name))
    else:
        outfile = open(os.path.join(dir, name), "wb")        
        outfile.write(zip.read(name))        
        outfile.close()
zipf.close()

os.chdir("G3D")
(build_out,build_in) = popen2.popen2("./buildg3d lib")
for line in build_out:
    print line
os.chdir("..")

print "Connecting to http://downloads.sourceforge.net"
response = urllib2.urlopen("http://downloads.sourceforge.net/g3d-cpp/G3D-7.00-data.zip")
print "Connected"
g3d_src = open("G3D-7.00-data.zip","wb")
print "Downloading G3D Data pack. Will take awhile.."
g3d_src.write( response.read() )
g3d_src.close()
print "Downloaded."
response.close()
dir = os.getcwd()
zipf = open("G3D-7.00-data.zip", "rb")
zip = zipfile.ZipFile(zipf)
for name in zip.namelist():
    print "Extracting " + name
    if name.endswith('/'):
        os.mkdir(os.path.join(os.getcwd(),name))
    else:
        outfile = open(os.path.join(dir, name), "wb")        
        outfile.write(zip.read(name))        
        outfile.close()
zipf.close()
# TO DO .. copy dlls to exe dir
